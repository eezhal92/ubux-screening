# ubux screening test

Simple Event RESTful API

## Requirements
- Yarn
- Node >= 8.11
- MongoDB

## Seed user data
It will create a user data with this credential:
 - email: johndoe@gmail.com
 - password: password

This credential can be used for login

```sh
yarn seed
```

## Running the project on local machine

- Install all dependencies

```sh
yarn install
```

- Run mongodb server
```sh
mongod
```

- In other terminal session, run the app
```sh
yarn start
```

## Running test
```sh
yarn test
```

## Available Endpoints

### `POST /api/auth/login`

Log user in. It will return jwt token.

| field           | value    | possible values                   |
|-----------------|----------|-----------------------------------|
| email           | string   | any string. eg: johndoe@gmail.com |
| password        | string   | any string. eg: password          |

### `GET /api/events`

Search events based on query strings

#### Available query strings

| query     | value  | possible values                               |
|-----------|--------|-----------------------------------------------|
| name      | string | any string.eg: john                           |
| filter    | string | all | past                                    |
| startDate | string | date iso string. eg: 2019-04-19T15:59:22.074Z |
| endDate   | string | date iso string. eg: 2019-04-19T15:59:22.074Z |

### `POST /api/events`

Create new event

#### Required request header

| header        | value  | value example                                 |
|---------------|--------|-----------------------------------------------|
| Authorization | string | Bearer your-auth-token                        |

#### Request body

| field           | value    | possible values                               |
|-----------------|----------|-----------------------------------------------|
| name            | string   | any string.eg: Awesome Event                  |
| owner           | string   | all | past                                    |
| usersInTheEvent | string[] | array of string of user id                    |
| location        | string   | eg: Palo Alto                                 |
| startDateTime   | string   | date iso string. eg: 2019-04-19T15:59:22.074Z |
| endDateTime     | string   | date iso string. eg: 2019-04-19T15:59:22.074Z |

### `PATCH /api/events/:id`

Update existing event

#### Required request header

| header        | value  | value example                                 |
|---------------|--------|-----------------------------------------------|
| Authorization | string | Bearer your-auth-token                        |


#### Request body

| field           | value    | possible values                               |
|-----------------|----------|-----------------------------------------------|
| name            | string   | any string.eg: Awesome Event                  |
| owner           | string   | all | past                                    |
| usersInTheEvent | string[] | array of string of user id                    |
| location        | string   | eg: Palo Alto                                 |
| startDateTime   | string   | date iso string. eg: 2019-04-19T15:59:22.074Z |
| endDateTime     | string   | date iso string. eg: 2019-04-19T15:59:22.074Z |

### `DELETE /api/events/:id`

Delete existing event

#### Required request header

| header        | value  | value example                                 |
|---------------|--------|-----------------------------------------------|
| Authorization | string | Bearer your-auth-token                        |
