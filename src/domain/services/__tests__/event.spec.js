import EventService from '../event'

function setup () {
  const service = new EventService()
  const event = {
    owner: 'user-a'
  }

  return { service, event }
}

describe('EventService', () => {
  describe('isTheOwner', () => {
    it('should return true when user owns the event', () => {
      const { service, event } = setup()

      expect(service.isTheOwner(event, 'user-a')).toBe(true)
    })

    it('should return false when user owns the event', () => {
      const { service, event } = setup()

      expect(service.isTheOwner(event, 'user-b')).toBe(false)
    })
  })

  describe('buildSearchCriteria', () => {
    it('should return correct search criteria', () => {
      const { service } = setup()
      const criteriaA = service.buildSearchCriteria({
        name: 'john'
      })

      expect(criteriaA.name instanceof RegExp).toBe(true)
      expect(criteriaA.startDateTime).toBe(undefined)

      const criteriaB = service.buildSearchCriteria({
        startDate: '2019-04-19T15:59:22.074Z',
        endDate: '2019-04-20T15:59:22.074Z'
      })

      expect(criteriaB.name instanceof RegExp).toBe(false)
      expect(criteriaB.startDateTime.$gte).toBe('2019-04-19T15:59:22.074Z')
      expect(criteriaB.startDateTime.$lte).toBe('2019-04-20T15:59:22.074Z')

      service._getNow = jest.fn(() => 'mocked-now')

      const criteriaC = service.buildSearchCriteria({
        filter: 'past'
      })

      expect(criteriaC.name instanceof RegExp).toBe(false)
      expect(criteriaC.startDateTime).toBe(undefined)
      expect(criteriaC.endDateTime.$lt).toBe('mocked-now')

      // If filter is set to past, it will ignore startDate and endDate
      // passed into the method. And only care about past event
      const criteriaD = service.buildSearchCriteria({
        name: 'John',
        startDate: '2019-04-19T15:59:22.074Z',
        endDate: '2019-04-20T15:59:22.074Z',
        filter: 'past'
      })

      expect(criteriaD.name instanceof RegExp).toBe(true)
      expect(criteriaD.startDateTime).toBe(undefined)
      expect(criteriaD.endDateTime.$lt).toBe('mocked-now')
    })
  })
})
