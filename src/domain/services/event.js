export default class EventService {
  isTheOwner (event, userId) {
    return event.owner.toString() === userId
  }

  _getNow () {
    return new Date().toISOString()
  }

  buildSearchCriteria (data) {
    const { name, startDate, endDate, filter } = data
    const criteria = {}

    if (name) {
      criteria.name = new RegExp(`${name}`, 'i')
    }

    if (startDate && endDate) {
      criteria.startDateTime = {
        $gte: startDate,
        $lte: endDate
      }
    }

    const isValidFilter = ['all', 'past'].includes(filter)
    if (isValidFilter) {
      delete criteria.startDateTime
      criteria.endDateTime = {
        $lt: this._getNow()
      }
    }

    return criteria
  }
}
