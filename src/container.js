import { createContainer, asValue, asClass, asFunction } from 'awilix'

import app from './app'
import config from './config'
import database from './infra/database'
import server from './http/server'
import router from './http/router'
import * as models from './infra/database/models'

import EventService from './domain/services/event'

const container = createContainer()

container.register({
  app: asFunction(app).singleton(),
  database: asFunction(database).singleton(),
  server: asFunction(server).singleton(),
  config: asFunction(config).singleton(),
  router: asFunction(router).singleton(),
  models: asValue(models),
  eventService: asClass(EventService).singleton()
})

export default container
