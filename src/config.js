export default function createConfig () {
  return {
    port: 3000,
    db: {
      uri: 'mongodb://localhost:27017/ubux-screening'
    }
  }
}
