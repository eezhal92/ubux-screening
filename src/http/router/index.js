import { Router } from 'express'
import createEventRouter from './event'
import createAuthRouter from './auth'

export default function createRouter (container) {
  const router = Router()

  router.use('/events', createEventRouter(container))
  router.use('/auth', createAuthRouter(container))

  return router
}
