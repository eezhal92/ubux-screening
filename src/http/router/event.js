import { Router } from 'express'

import createEventRepo from '../../infra/repo/event'
import { shouldAuthenticated } from '../middleware/auth'
import * as eventUseCases from '../../app/usecases/event'
import { createInputValidation } from '../middleware/input'

function createEventRouter (container) {
  const router = Router()
  const { eventService } = container
  const { Event: EventModel } = container.models
  const eventRepo = createEventRepo(EventModel)
  const createEventUseCase = eventUseCases.create({ eventRepo })
  const removeEventUseCase = eventUseCases.remove({ eventRepo, eventService })
  const updateEventUseCase = eventUseCases.update({ eventRepo, eventService })
  const searchEventUseCase = eventUseCases.search({ eventRepo, eventService })

  router.post(
    '/',
    createInputValidation({
      name: 'required|min:3',
      startDateTime: 'required|date',
      endDateTime: 'required|date',
      location: 'required',
      usersInTheEvent: 'array'
    }),
    shouldAuthenticated,
    (request, response) => {
      const data = request.body

      createEventUseCase.exec({
        ...data,
        owner: request.userId
      })
        .then((event) => {
          response.json({
            event
          })
        })
    }
  )

  router.delete('/:id', (request, response) => {
    removeEventUseCase.exec({
      eventId: request.params.id,
      actorId: request.userId
    })
      .then(() => {
        response.json({
          message: 'Event has been deleted'
        })
      })
  })

  router.patch(
    '/:id',
    createInputValidation({
      name: 'required|min:3',
      startDateTime: 'required|date',
      endDateTime: 'required|date',
      location: 'required',
      usersInTheEvent: 'array'
    }),
    shouldAuthenticated,
    (request, response) => {
      const data = request.body

      updateEventUseCase.exec({
        eventId: request.params.id,
        actorId: request.userId,
        payload: data
      })
        .then((event) => {
          response.json({
            event
          })
        })
        .catch(err => {
          const statusCode = err.statusCode || 500

          response.status(statusCode).json({
            message: err.message
          })
        })
    }
  )

  router.get('/', (request, response) => {
    searchEventUseCase.exec(request.query)
      .then((events) => {
        response.json({
          events
        })
      })
  })

  return router
}

export default createEventRouter
