import { Router } from 'express'

import createUserRepo from '../../infra/repo/user'
import * as authUseCases from '../../app/usecases/auth'

function createAuthRouter (container) {
  const router = Router()
  const { User: UserModel } = container.models
  const userRepo = createUserRepo(UserModel)
  const userLoginUseCase = authUseCases.login({ userRepo })

  router.post('/login', (request, response) => {
    const data = request.body

    userLoginUseCase.exec(data)
      .then((data) => {
        response.json(data)
      })
      .catch(err => {
        const statusCode = err.statusCode || 500
        response.status(statusCode).json({
          message: err.message
        })
      })
  })

  return router
}

export default createAuthRouter
