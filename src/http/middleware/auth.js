import jwt from 'jsonwebtoken'

/**
 * @param {import("express").Request}      request
 * @param {import("express").Response}     response
 * @param {import("express").NextFunction} next
 */
export function shouldAuthenticated (request, response, next) {
  const authHeader = request.header('Authorization')

  if (!authHeader) {
    return next(new Error('You are not authenticated'))
  }

  const [, token] = authHeader.split('Bearer ')
  let decoded = null
  try {
    decoded = jwt.verify(token, 'hardcodedsecret')
  } catch (error) {
    return next(error)
  }

  request.userId = decoded.id

  next()
}
