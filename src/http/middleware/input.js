import Validator from 'validatorjs'

export function createInputValidation (rules) {
  return function shouldHaveValidInput (request, response, next) {
    const validation = new Validator(request.body, rules)

    if (validation.fails()) {
      const error = new Error('Your input is not valid')
      error.errors = validation.errors

      return next(error)
    }

    next()
  }
}
