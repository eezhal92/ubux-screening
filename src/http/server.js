import express from 'express'
import bodyParser from 'body-parser'

export default function createServer ({ config, router }) {
  const app = express()

  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use('/api', router)
  app.use((error, request, response, next) => {
    if (error.errors) {
      return response.status(422).json(error.errors)
    }

    next(error)
  })

  function start () {
    return new Promise((resolve) => {
      app.listen(config.port, () => {
        console.log(`Listening on PORT: ${config.port}`)
        resolve()
      })
    })
  }

  return {
    app,
    start
  }
}
