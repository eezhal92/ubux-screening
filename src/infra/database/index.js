import mongoose from 'mongoose'

export default function createDatabase ({ config }) {
  function connect () {
    return mongoose
      .connect(
        config.db.uri,
        { useNewUrlParser: true }
      )
      .then(self => self.connection)
  }

  function disconnect (connection) {
    return connection.close()
  }

  return {
    connect,
    disconnect
  }
}
