/**
 *
 * @param {import("mongoose").Model} model
 */
export default function createEventRepo (model) {
  function create (event) {
    return model.create(event)
  }

  function removeById (id) {
    return model.findOneAndRemove({ _id: id })
  }

  function findById (id) {
    return model.findById(id)
  }

  function update (eventId, data) {
    return model.findByIdAndUpdate(eventId, data, {
      new: true
    })
  }

  function search (criteria) {
    return model.find(criteria)
  }

  return {
    create,
    update,
    search,
    findById,
    removeById
  }
}
