export default function createUserRepo (model) {
  /**
   * @param {object} payload
   * @param {string} payload.email
   * @param {string} payload.password
   */
  function findByEmailAndPassword (payload) {
    return model.findOne(payload)
  }

  /**
   * @param {string} email
   */
  function findByEmail (email) {
    return model.findOne({ email })
  }

  return {
    findByEmail,
    findByEmailAndPassword
  }
}
