import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

export function verifyPassword (passwordInQuestion, hashedPassword) {
  return bcrypt.compareSync(passwordInQuestion, hashedPassword)
}

export function createToken ({ _id, name, email }) {
  return jwt.sign({ id: _id, name, email }, 'hardcodedsecret')
}

export function createNotFoundError () {
  const error = new Error('User was not found')
  error.statusCode = 404

  return error
}
