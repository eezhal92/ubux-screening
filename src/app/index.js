export default function createApp ({ server, database }) {
  function start () {
    return Promise.resolve()
      .then(database.connect)
      .then(server.start)
  }
  return {
    start
  }
}
