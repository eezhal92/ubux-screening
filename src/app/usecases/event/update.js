export default function updateEventUseCase ({ eventRepo, eventService }) {
  /**
   * @param {object}   data
   * @param {string}   data.eventId
   * @param {string}   data.actorId
   * @param {string}   data.payload
   * @param {string}   data.payload.name
   * @param {string}   data.payload.startDateTime
   * @param {string}   data.payload.endDateTime
   * @param {string[]} data.payload.usersInTheEvent
   * @param {string}   data.payload.location
   */
  async function exec (data) {
    const { payload, eventId, actorId } = data
    const event = await eventRepo.findById(eventId)

    if (!event) {
      const error = new Error('Event was not found')
      error.statusCode = 404

      throw error
    }

    if (!eventService.isTheOwner(event.toJSON(), actorId)) {
      const error = new Error('Actor is not the owner')
      error.statusCode = 403

      throw error
    }

    return eventRepo.update(eventId, payload)
  }

  return {
    exec
  }
}
