/**
 * @param {object} options
 */
export default function removeEventUseCase ({ eventRepo, eventService }) {
  /**
   * @param {object} data
   * @param {string} data.eventId
   * @param {string} data.actorId
   */
  async function exec (data) {
    const event = await eventRepo.findById(data.eventId)

    if (!event) throw new Error('Not found')

    if (!eventService.isTheOwner(event.toJSON(), data.actorId)) {
      throw new Error('Actor is not the owner')
    }

    return eventRepo.removeById(data.eventId)
  }

  return {
    exec
  }
}
