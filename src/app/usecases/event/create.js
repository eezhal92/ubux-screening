export default function createEventUseCase ({ eventRepo }) {
  /**
   * @param {object}   data
   * @param {string}   data.name
   * @param {string}   data.startDateTime
   * @param {string}   data.endDateTime
   * @param {string[]} data.usersInTheEvent
   * @param {string}   data.location
   * @param {string}   data.owner
   */
  function exec (data) {
    return eventRepo.create(data)
  }

  return {
    exec
  }
}
