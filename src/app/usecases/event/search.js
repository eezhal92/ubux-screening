export default function searchEventUseCase ({ eventRepo, eventService }) {
  /**
   * @param {object}   data
   * @param {string}   data.name
   * @param {string}   data.startDate
   * @param {string}   data.endDate
   * @param {string}   data.filter   possible values: all | past
   */
  function exec (data) {
    const criteria = eventService.buildSearchCriteria(data)

    return eventRepo.search(criteria)
  }

  return {
    exec
  }
}
