import * as auth from '../../utils/auth'

export default function userLoginUseCase ({ userRepo }) {
  /**
   * @param {object} data
   * @param {object} data.email
   * @param {object} data.password
   */
  async function exec (data) {
    const user = await userRepo.findByEmail(data.email)

    if (!user) {
      throw auth.createNotFoundError()
    }

    const verified = auth.verifyPassword(data.password, user.password)

    if (!verified) {
      throw auth.createNotFoundError()
    }

    const token = auth.createToken(user)

    return { token }
  }

  return {
    exec
  }
}
