import bcrypt from 'bcrypt'
import container from './container'

const database = container.resolve('database')
const models = container.resolve('models')

function removeAllUser () {
  console.log('Removing all user...')
  return models.User.deleteMany({})
}

function createAUser () {
  console.log('Creating a user...')
  const salt = bcrypt.genSaltSync(10)
  const password = bcrypt.hashSync('password', salt)

  return models.User.create({
    name: 'John Doe',
    password,
    email: 'johndoe@gmail.com'
  })
}

async function seedUser () {
  const connection = await database.connect()

  await removeAllUser()
  await createAUser()

  database.disconnect(connection)

  console.log('User collection has been seeded!')
}

seedUser()
